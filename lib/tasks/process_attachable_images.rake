require_relative '../../config/environment'

task process_attachable_images: :environment do
  ImageProcessor.process(period_number: 1, period_unit: :hour)
end

task process_all_attachable_images: :environment do
  ImageProcessor.process(period_number: nil, period_unit: nil)
end