// custom options: enablejsapi: 1, origin to current protocol/host
var $homeVideoButton = $(".company_video_btn")
$homeVideoButton.modalVideo({
	iframe_id: 'company-video-iframe',
	youtube: {
		autoplay: 1,
		cc_load_policy: 1,
		controls: 1,
		disablekb: 0,
		enablejsapi: 1,
		fs: 1,
		iv_load_policy: 1,
		loop: 0,
		mute: 0,
		origin: location.origin,
		rel: 0,
		showinfo: 1,
		start: 0,
		wmode: 'transparent',
		theme: 'dark',
		nocookie: false}
});

function initializeYoutubeApi() {
	window.onYouTubeIframeAPIReady = function() {
		var player = new YT.Player('company-video-iframe', {
			events: {
				'onReady': function (event) {
					console.log("onReady")
					event.target.playVideo();
				},
			}
		})

		// player = new YT.Player('player', {
		// 	height: '390',
		// 	width: '640',
		// 	videoId: 'M7lc1UVf-VE',
		// 	events: {
		// 		'onReady': onPlayerReady,
		// 		'onStateChange': onPlayerStateChange
		// 	}
		// });
	}
}

function useYoutubeApi(handler) {
	if (window.YT) {
		handler()
	}
	else {
		setTimeout(100, useYoutubeApi.bind(this, handler))
	}
}

if ( $("body").data('hasYoutubeVideo') !== undefined ) {
	//initializeYoutubeApi()
	$homeVideoButton.click(function() {
		useYoutubeApi(function () {
			console.log("using youtube api in my handler")
			var player = new YT.Player('company-video-iframe', {
				events: {
					'onReady': function (event) {
						console.log("onReady")
						//alert("onReady")
						setTimeout(function () {
							event.target.playVideo();
						}, 500)
					},
					'onStateChange': function (event) {
						console.log('onStateChange: event: ', event)
					}
				}
			})
		})
	})
}
