#     P L U G I N S

#= require plugins/scroll-banner
#= require plugins/jquery-modal-video.custom
#= require plugins/jquery.drawsvg
#= require plugins/isMobile.min

#     I N I T I A L I Z E

#= require fullpage_banner_height
#= require home_banner_animation
#= require base_video_popup
#= require home_video_popup
#= require home_bg_video