#     P L U G I N S

#= require plugins/lightgallery.min
#= require plugins/lg-share
#= require lg-thumbnail.custom
#= require plugins/jquery-modal-video.custom

#     I N I T I A L I Z E

#= require lightgallery
#= require froogaloop
#= require base_video_popup
#= require modal_video_popup
#= require pinterest_btn