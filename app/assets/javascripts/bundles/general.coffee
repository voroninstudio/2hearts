#= require jquery.js
# require jquery-ui
# require jquery_ujs

#= require global

#     P L U G I N S
#= require plugins/jquery-easing
#= require plugins/jquery.appear
#= require plugins/lazyload.min
#= require plugins/jquery.bxslider
#= require plugins/owl.carousel.min

#     I N I T I A L I Z E

#= require basic
#= require lazyload-initialize
#= require appear-initialize
#= require bxslider
#= require owl-carousel
#= require menu
#= require bg-text

