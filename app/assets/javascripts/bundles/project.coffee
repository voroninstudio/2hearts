#= require images.js.erb

#     P L U G I N S

#= require plugins/lightgallery.min
#= require plugins/lg-share
#= require lg-thumbnail.custom
#= require plugins/jquery-modal-video.custom
#= require js-cookie/src/js.cookie
#= require plugins/jquery.drawsvg

#     I N I T I A L I Z E

#= require project_banner_animation
#= require lightgallery
#= require froogaloop
#= require base_video_popup
#= require modal_video_popup
#= require sharing
#= require pinterest_btn
#= require likes