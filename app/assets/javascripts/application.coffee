#= require jquery.js
#= require jquery-ui
#= require jquery_ujs
#= require plugins/isMobile.min

#= require global
#= require images.js.erb

#     P L U G I N S

#= require plugins/jquery-easing
#= require plugins/jquery.appear
#= require plugins/clickout
#= require plugins/form
#= require plugins/jquery.bxslider
#= require plugins/jquery.scrolldelta
#= require plugins/scroll-banner
#= require plugins/owl.carousel.min
#= require plugins/jquery.drawsvg
#= require plugins/lightgallery.min
#= require plugins/lg-share
#= require plugins/lg-thumbnail.min
#= require jquery-form/dist/jquery.form.min
#= require plugins/jquery-modal-video.custom
#= require js-cookie/src/js.cookie
#= require plugins/lazyload.min

#     I N I T I A L I Z E

#= require basic
#= require lazyload-initialize
#= require appear-initialize
#= require bxslider
#= require owl-carousel
#= require fullpage_banner_height
#= require header
#= require menu
#= require accordion
#= require popups
#= require filters
#= require home_banner_animation
#= require project_banner_animation
#= require mask
#= require lightgallery
#= require input-file-add
#= require froogaloop
#= require base_video_popup
#= require modal_video_popup
#= require bg-text
#= require scrollBtn

#= require sharing
#= require pinterest_btn

#= require form
#= require email_subscription_form
#= require likes