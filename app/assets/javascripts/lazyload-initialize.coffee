initializeLazyLoad = ()->
  myLazyLoad = new LazyLoad({
    elements_selector: ".lazy"
  });

initializeLazyLoad()