$(document).on('click', '.project_video_button', function () {
	var videoPopupId = $(this).attr("data-video-popup-id")
	openVimeoVideoPopup(videoPopupId)
})

$(document).on('click', '.modal-video.opened', function (event) {
	var $target = $(event.target)
	var shouldClose = $target.hasClass('modal-video') || $target.hasClass('modal-video-inner') || $target.hasClass('modal-video-close-btn')
	if ( shouldClose ) {
		var $modal = $(this)
		var videoPopupId = $modal.attr('id')
		var player = playersByPopupId[videoPopupId]
		player.api('pause')
		$modal.removeClass('opened')
	}
})