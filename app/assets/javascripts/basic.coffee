page_loaded_handler = ()->
  setTimeout(
    ()->
      $('#loader-wrapper').fadeOut(
        complete: ()->
          # run some animations
          #$.force_appear()
          #$window.trigger("after_preloader")
      )
    100
  )

loadScript = (url, onload_handler)->
  wf = document.createElement('script');
  s = document.scripts[0];
  wf.src = url;
  wf.async = true;
  wf.defer = true;
  if onload_handler
    wf.onload = onload_handler
  s.parentNode.insertBefore(wf,s);
window.addEventListener('load', page_loaded_handler)
window.addEventListener('load', ()->
  loadScript(
    '//ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js',
    ()->
      google_font_families = JSON.parse($body.attr("data-google-font-families"))
      for f in google_font_families
        WebFont.load({
          google: {
            families: [f]
          }
        })
  )

  #loadScript('//www.youtube.com/iframe_api')
  if $body[0].hasAttribute('data-load-pinterest')
    loadScript('//assets.pinterest.com/js/pinit.js')
  loadScript('//api.venyoo.ru/wnew.js?wc=venyoo/default/science&widget_id=4812314439843840')
  ga_id = $body.attr('data-gtm-id')
  if ga_id && ga_id.length
    loadScript("https://www.googletagmanager.com/gtag/js?id=#{ga_id}")
    window.dataLayer = window.dataLayer||[];
    gtag = ->
      dataLayer.push(arguments)
    gtag('js',new Date());
    gtag('config',ga_id);
)