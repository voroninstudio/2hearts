window.playersByPopupId = {}

window.openVimeoVideoPopup = function(videoPopupId) {
	var $modal = $('#' + videoPopupId)
	$modal.addClass('opened')

	var player;
	if (!playersByPopupId[videoPopupId]) {
		var iframe = $modal.find("iframe")[0]
		player = $f(iframe);
		playersByPopupId[videoPopupId] = player
	}
	else {
		player = playersByPopupId[videoPopupId]
		player.api("seekTo", 0);
	}

	player.api("play");
}

window.openYoutubeVideoPopup = function (videoPopupId) {
	var $modal = $('#' + videoPopupId)
	$modal.addClass('opened')
}