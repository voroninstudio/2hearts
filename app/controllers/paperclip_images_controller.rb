class PaperclipImagesController < ActionController::Base
  def attachable_asset
    id = (params[:id1] + params[:id2] + params[:id3]).to_i
    style = params[:style].to_sym
    filename = params[:base_filename] + '.' + params[:format]

    begin
      asset = Attachable::Asset.find(id)
      attachment = asset.data
      attachment_file_name = asset.data_file_name
      attachment_content_type = attachment.content_type
      if filename.downcase == attachment_file_name.downcase && attachment.exists?
        #render json: {id: id, style: style, filename: filename}
        asset.set_attachment_styles(IMAGE_OPTIONS[:styles])
        asset.data.reprocess!(style)
        send_file attachment.path(style), url_based_filename: true, disposition: 'inline', :type => attachment_content_type
      else
        render_not_found
      end
    rescue ActiveRecord::RecordNotFound
      render_not_found
    end
  end

  private

  def render_not_found
    @render_footer = false
    render template: "errors/not_found.html.slim", status: 404
  end
end