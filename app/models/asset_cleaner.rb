class AssetCleaner
  SCHEMES = [
    [:large, :square, :square, :medium_tall, :square, :square],
    [:medium_square, :medium_tall, :medium_tall, :medium_square],
    [:large, :wide, :small_square, :small_square, :medium_square],
    10.times.map { :square },
    [:large, :square, :square, :square, :square, :square],
    [:large],
    [:large, :wide, :small_square, :small_square, :small_square, :small_square, :wide],
    [:large, :square, :square]
  ]
  def models_with_attachable_images
    Cms.active_record_models.select do |model|
      model.reflections.any? { |reflection_key, reflection| reflection.options[:class_name] == 'Attachable::Asset' }
    end
  end

  def attachable_association_names_for_model(model)
    model.reflections
      .select { |reflection_key, reflection| reflection.options[:class_name] == 'Attachable::Asset' }
      .map { |reflection_key, reflection| reflection_key }
  end

  def clean_unused_attachable_image_styles_for_all
    models_with_attachable_images.each do |model|
      association_names = attachable_association_names_for_model(model)
      model.all.each do |model_instance|
        association_names.each do |association_name|
          scheme_index = model_instance.send("#{association_name}_scheme").try {|n| n.to_i - 1 }
          next unless scheme_index
          assets = model_instance.send(association_name)
          assets.each_with_index do |asset, asset_index|
            needed_styles = [:original, :thumb] + calculate_used_styles_scheme_image(scheme_index, asset_index)
            not_needed_styles = IMAGE_OPTIONS[:styles].keys.select { |k| !needed_styles.include?(k) }
            puts "asset_id: #{asset.id} needed_styles: #{needed_styles} ; NOT_NEEDED_STYLES: #{not_needed_styles};"
            not_needed_styles.each do |style_name|
              style_image_path = path_for_attachment_style(asset.data, style_name)
              dir_path = File.dirname(style_image_path)
              if File.exists?(dir_path)
                puts "remove directory: #{dir_path}"
                FileUtils.rm_r(dir_path)
              else
                puts "skip: directory does not exist: #{dir_path}"
              end
            end
          end
        end
      end
    end

    nil
  end

  def calculate_used_styles_scheme_image(scheme_index, attachment_index)
    styles = [:popup]
    attachment_index = attachment_index % (SCHEMES[scheme_index].count)
    styles << SCHEMES[scheme_index][attachment_index]

    styles
  end

  def path_for_attachment_style(attachment, style_name)
    path_pattern = Attachable::Asset.attachment_definitions[:data][:path]
    attachment.send(:interpolate, path_pattern, style_name)
  end

  def styles_for_attachment(attachment)

  end
end