class ImageProcessor
  def self.process(period_number: 1, period_unit: :hour)
    assets = Attachable::Asset.unscoped.order(id: :desc)
    if period_number && period_unit
      assets = assets.where("updated_at > ?", Time.zone.now - period_number.send(period_unit))
    end

    if assets.present?
      puts "total images count: #{Attachable::Asset.count}"
      puts "total images count since last #{period_number} #{period_unit}: #{assets.count}"
      puts "assets: #{Attachable::Asset::Config.default_styles}"
      assets.each_with_index do |asset, i|
        puts "Attachable::Asset id: #{asset.id}, index: #{i}"
        asset.create_non_existing_versions
      end
      puts "======================"
      puts "Images processed. Clear html pages cache"

      Cms::Caching.clear_cache
      puts "Complete"
    else
      if period_number && period_unit
        period_str = " since last #{period_number} #{period_unit}"
      else
        period_str = ''
      end

      puts "You have no created/updated images#{period_str}. Exit"
    end
  end
end