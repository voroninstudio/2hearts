class PrivacyPolicyPageInfo < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :content

  default_scope do
    order('id desc')
  end

  has_cache do
    pages :privacy_policy
  end

end