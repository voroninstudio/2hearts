require_relative 'paperclip'
Cms::CompressionConfig.initialize_compression(html_compress: false)
if !ENV["STD_PRECOMPILE"]
  Cms::AssetsPrecompile.initialize_precompile
end

Cms.config do |config|
  config.provided_locales do
    [:uk, :ru, :en]
  end

  config.visible_locales_for_navigation do
    [:uk]
  end

  config.inline_svg_allow_override_size false

  config.use_translations true
end

IMAGE_OPTIONS = {styles: { thumb: "200x200#", large: "1380x700#", square: "450x450#", wide: "900x450#", medium_square: "680x680#", medium_tall: "680x1040#", small_wide: "680x360#", small_square: "330x330#", popup: {geometry: "1920x1080>", **WATERMARK_ORIGINAL_STYLE} }}
#Attachable::Asset::Config.default_styles = IMAGE_OPTIONS[:styles]
Attachable::Asset::Config.default_styles = ENV['PROCESSING_IMAGES'].present? ? IMAGE_OPTIONS[:styles] : {}
