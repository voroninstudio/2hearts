class CreatePrivacyPolicyPageInfos < ActiveRecord::Migration
  def change
    create_table :privacy_policy_page_infos do |t|
      t.text :content
      t.timestamps null: false
    end
    create_translation_table(PrivacyPolicyPageInfo, :content)
  end
end