class ChangePhotoAndVideoColumnsForProjects < ActiveRecord::Migration
  def change
    rename_column :project_translations, :photo_and_video, :photographer
    rename_column :projects, :photo_and_video, :photographer
    add_column :project_translations, :videographer, :string
    add_column :projects, :videographer, :string
  end
end
